# SPDX-FileCopyrightText: 2021 René de Hesselle <dehesselle@web.de>
#
# SPDX-License-Identifier: GPL-2.0-or-later

name: toolset_build
on:
  workflow_call:
    inputs:
      SDK_DOWNLOAD_URL_REQUIRED:
        description: "If set to true, break the build on missing SDK_DOWNLOAD_URL."
        default: false
        required: false
        type: boolean
      WRK_DIR:
        description: "Location of our work directory."
        default: /Users/Shared/work
        required: false
        type: string
      CCACHE_DIR:
        description: "Location of our ccache directory."
        default: /Users/Shared/work/ccache
        required: false
        type: string
    secrets:
      SDK_DOWNLOAD_URL:
        required: false
jobs:

  ##############################################################################

  toolset_build:
    runs-on: macos-12
    env:
      WRK_DIR: ${{ inputs.WRK_DIR }}
      CCACHE_DIR: ${{ inputs.CCACHE_DIR }}
    steps:

      #------------------------------------------------- prepare the environment

      - name: checkout mibap repository
        uses: actions/checkout@v3
        with:
          submodules: recursive

      - name: create cache id
        id: cache_id
        uses: josStorer/get-current-time@v2.0.2
        with:
          format: "YYYY-MM-DD-HH-mm-ss"

      # Create a new cache, building ontop the most recent old one.
      - name: setup cache
        uses: actions/cache@v3
        with:
          path: ${{ env.CCACHE_DIR }}
          key: ccache-toolset-${{ steps.cache_id.outputs.formattedTime }}
          restore-keys: ccache-toolset-

      # GitHub does not provide 10.13 SDK on their runners and no image older
      # than Catalina. See here for what you can expect in their images:
      # https://github.com/actions/virtual-environments/tree/main/images/macos
      #
      # Official downloads from Apple are not accessible without a developer
      # account and I don't trust 3rd party sources (e.g. "phracker"). So I'm
      # using my own (non-public) source, but I'm providing the means to verify
      # its genuinity, see here:
      # https://github.com/dehesselle/sdkchecksum
      #
      # In order to use your own SDK_DOWNLOAD_URL, create a repository secret
      # of said name and proivide a link to a .tar.xz file.

      - name: install macOS SDK
        if: env.SDK_DOWNLOAD_URL != null
        env:
          SDK_DOWNLOAD_URL: ${{ secrets.SDK_DOWNLOAD_URL }}
        run: |
          mkdir -p $WRK_DIR
          curl --http1.1 -L ${{ secrets.SDK_DOWNLOAD_URL }} | tar -C $WRK_DIR -xJp
          echo "SDKROOT=$WRK_DIR/$(basename ${SDK_DOWNLOAD_URL%%.tar.xz*})" >> $GITHUB_ENV

      - name: verify SDK
        if: env.SDK_DOWNLOAD_URL != null
        env:
          SDK_DOWNLOAD_URL: ${{ secrets.SDK_DOWNLOAD_URL }}
        uses: dehesselle/sdkchecksum@master
        with:
          SDKROOT: ${{ env.SDKROOT }}
          SHA256_FILE: MacOSX10.13.4.sdk.sha256

      # GitHub does not provide a clean macOS installation. We need to move the
      # pre-installed components out of the way so we don't pick them up by
      # accident.
      - name: disable /usr/local
        run: |
          cd /usr/local
          for dir in include lib share; do sudo mv $dir $dir.disabled; done

      #---------------------------------------------- run the actual build steps

      - name: bootstrap JHBuild
        run: ./110-bootstrap_jhb.sh

      - name: build GTK 3 stack
        run: ./120-build_gtk3.sh

      - name: build Inkscape dependencies
        run: ./130-build_inkdeps.sh

      - name: build packaging related components
        run: ./140-build_packaging.sh

      #------------------------------------------------- create toolset artifact

      - name: remove non-essential files
        run: |
          source jhb/etc/jhb.conf.sh
          find "$SRC_DIR" -mindepth 1 -maxdepth 1 -type d ! -name 'bash_d' ! -name 'gtk-mac-bundler*' ! -name 'jhb*' -exec rm -rf {} \;
          rm -rf "${BLD_DIR:?}"/*
          rm -rf "${TMP_DIR:?}"/*

      - name: create toolset dmg
        id: toolset
        run: |
          jhb/usr/bin/archive create_dmg
          source jhb/etc/jhb.conf.sh
          echo "dmg=$RELEASE_ARCHIVE" >> $GITHUB_OUTPUT

      # Restore /usr/local, GitHub actions depend on these.
      - name: restore /usr/local
        run: for dir in /usr/local/*.disabled; do sudo mv $dir ${dir/.disabled/}; done

      - name: upload toolset disk image
        uses: actions/upload-artifact@v3
        with:
          name: Inkscape_build_toolset
          path: ${{ steps.toolset.outputs.dmg }}

      - name: upload toolset checksum
        uses: actions/upload-artifact@v3
        with:
          name: Inkscape_build_toolset.sha256
          path: ${{ steps.toolset.outputs.dmg }}.sha256
